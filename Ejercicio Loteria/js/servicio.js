class Boleto {
    //Constructor
    constructor(fecha) {
        this.fechaEmision = fecha;
        this.numBoleto = this.generarNumBoleto()
    }

    //Método que nos generará el número aleatorio
    generarNumBoleto() {
        return Math.floor(Math.random() * 99999 + 1);
    }


    //Método que nos devolvera el número del boleto
    get_NumBoleto() {
        return this.numBoleto;
    }
}