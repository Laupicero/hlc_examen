//-------------------------------------------
//Variable Global
var boletos = new Array();

// Se crea un nuevo boleto de la lotería y se almacena en el programa. 
function generarBoleto() {
    boleto = new Boleto(new Date());
    boletos.unshift(boleto);

    alert(`Boleto creado - ${boleto.get_NumBoleto()}`);
    //console.log(boleto);
}


// Sale un listado de los boletos pares que hemos almacenado.
function listarPares() {
    let lista = document.createElement(`ul`);
    var boletosPares = new Array();

    for (let i = 0; i < boletos.length; i++) {
        let num = boletos[i].numBoleto;
        if (num / 2 === 0) {
            boletosPares.unshift(boletos[i]);
            let listaItem = document.createElement(`li`);
            listaItem.innerText = boletos[i].get_NumBoleto();
            lista.appendChild(listaItem);
        }
    }

    let listadoImp = document.getElementById(`listadoPares`);
    listadoImp.appendChild(lista);
    listadoImp.className = `backRojo`;
}

// Sale un listado de los boletos impares que hemos almacenado.
function listarImpares() {
    let lista = document.createElement(`ul`);
    var boletosImpares = new Array();

    for (let i = 0; i < boletos.length; i++) {
        if (boletos[i].numBoleto / 2 !== 0) {
            boletosImpares.unshift(boletos[i]);
            let listaItem = document.createElement(`li`);
            listaItem.innerText = boletos[i].get_NumBoleto();
            lista.appendChild(listaItem);
        }
    }

    let listadoImp = document.getElementById(`listadoImpares`);
    listadoImp.appendChild(lista);
    listadoImp.className = `backVerde`;
}

function main() {
    document.getElementById('generar').addEventListener('click', generarBoleto);
    document.getElementById('pares').addEventListener('click', listarPares);
    document.getElementById('impares').addEventListener('click', listarImpares);

}
window.addEventListener('load', main);