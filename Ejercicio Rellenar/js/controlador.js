// A partir del esqueleto proporcionado consigue el siguiente comportamiento: 
// el objetivo es simplemente rellenar con color cada una de las celdas de la tabla incluida 
// cuando se pulsen con el ratón. Cuando se colorean todas, se gana la partida. 
// Es importante que cumplas los siguientes requisitos: 
//     - Utiliza delegación de eventos para identificar la casilla sobre la que dispara el usuario. 
//     -Debes crear una clase modelo que haga una representación interna del tablero de juego. 
//     Introduce en esa clase los métodos para seleccionar la casilla coloreada y para comprobar si se ha ganado. 
//     - En el archivo controlador.js debes gestionar los eventos, 
// consultar al modelo y dar feedback al usuario. 


//Var globales
var seleccion;
var tablero;


function main() {
    //No se pudo realizar, daba muchos fallos, y no obtenía el evento 'click' de la tabla:
    // ERROR: controlador.js:28 Uncaught TypeError: tabla.addEventListener is not a function at main 
    // let filas = document.getElementsByTagName(`tr`);
    // let columnas = parseInt(document.getElementsByTagName(`td`).length) / filas.length;

    // tablero = new Tablero(filas, columnas);

    // document.getElementsByTagName(`table`).addEventListener...;

    tablero = document.getElementsByClassName(`casilla`);

    for (let i = 0; i < tablero.length; i++) {
        tablero[i].addEventListener('click', function(e) {
            //console.log(e.target.ClassName);
            if (e.target.ClassName === "casilla") {
                e.target.ClassName = `casillaSeleccionada`;

                comprobarCasillas();
            }
        });
    }


}
window.addEventListener('load', main);

// Se realizará la búsqueda de alguna casilla con la clase 'casilla'
// Sino se encuentra ninguna el juego parará
function comprobarCasillas() {
    let finJuego = true;
    for (let i = 0; i < ) tablero.length;
    i++) {
    if (tablero[i].ClassName === `casilla`) {
        finJuego = false;
    }

    if (finJuego) {
        reseteoCasillas()
    }
}
}


function reseteoCasillas() {
    for (let i = 0; i < ) tablero.length;
    i++) {
    tablero[i].ClassName = `casilla`;
}
}